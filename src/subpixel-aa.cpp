/*  Sub-pixel Antialiasing Thing
    Copyright (C) 2020  dan9er
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <Magick++.h>

int main(int argc, char const *argv[])
{
    // init magick
    Magick::InitializeMagick(*argv);

    // read image
    Magick::Image inImg("/dev/stdin");

    // check size for multiple of 3
    if (inImg.columns() % 3)
        return 1;

    // init output image
    Magick::Image outImg(Magick::Geometry(inImg.columns()/3, inImg.rows()), Magick::Color("red"));

    // loop through pixels
    for (unsigned y = 0; y != outImg.rows(); ++y)
        for (unsigned x = 0; x != outImg.columns(); ++x)
        {
            // input x coord
            unsigned in_x = x*3;

            // paint the pixel
            outImg.pixelColor
            (
                x,
                y,
                Magick::ColorRGB
                (
                    Magick::ColorRGB
                        (inImg.pixelColor(in_x  ,y)).red(),
                    Magick::ColorRGB
                        (inImg.pixelColor(in_x+1,y)).green(),
                    Magick::ColorRGB
                        (inImg.pixelColor(in_x+2,y)).blue()
                )
            );
        }

    // write outImg
    outImg.write("png:/dev/stdout");

    return 0;
}
